#!/bin/bash

<<COMMENT
    Author: Brennen Murphy
    Date: 07/16/2018
    Version: 1.00
    GitLab: https://gitlab.com/krazynez
COMMENT


function main(){
clear
echo "==============================================="
echo "|                                             |"
echo "| 1.) DuckDuckGo        2.) GamingOnLinux     |"
echo "|                                             |"
echo "| 3.) Wololo            4.) LinuxToday        |"
echo "|                                             |"
echo "| 5.) Kernel            6.) RetroPie          |"
echo "|                                             |"
echo "| 7.) Youtube (subs)    8.) Youtube (no-subs) |"
echo "|                                             |"
echo "| 9. ) Exit                                   |"
echo "|                                             |"
echo "==============================================="


read -p "Choose an option: " ask



case "$ask" in
   
    [a-zA-Z]) echo -e "\nHa! You thought you could cause an infinite loop!"
	sleep 3
	main
	;;

    1)	gio open https://start.duckduckgo.com
	;;
    
    2)  gio open https://www.gamingonlinux.com
	;;

    3)  gio open http://wololo.net
	;;

    4)	gio open https://www.linuxtoday.com
	;;

    5)	gio open https://kernel.org
	;;

    6)	gio open https://retropie.org.uk
	;;

    7)	gio open https://www.youtube.com/feed/subscriptions/
	;;

    8)	gio open https://www.youtube.com/
	;;

    9)	exit 0;
	;;

    *)  echo "Invaild input..."
        sleep 3;
	main
   esac


}

main
